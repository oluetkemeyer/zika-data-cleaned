import numpy as np
import unidecode
import scipy as sp

locations = np.genfromtxt("locations.tsv", dtype=np.unicode, delimiter="\t")

region = np.array(["Country-State", "Geometry"])
for i in range(0, len(locations)):
    countryState = unidecode.unidecode(locations[i][2] + "-" + locations[i][4])
    geometry = locations[i][12]
    target = np.array([countryState, geometry])
    region = np.vstack((region, target))

np.savetxt("locations.csv", region, delimiter="\t", fmt='%s')



